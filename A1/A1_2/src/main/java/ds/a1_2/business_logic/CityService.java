package ds.a1_2.business_logic;

import java.util.List;

import ds.a1_2.data_access.CityDAO;
import ds.a1_2.model.City;

public class CityService {
private CityDAO cityDAO;
	
	public CityService(){
		cityDAO = new CityDAO();
	}
	
	public List<City> findAll(){
		return cityDAO.findAll();
	}
	
	public boolean createCity(City city){
		City savedcity = cityDAO.save(city);
		
		return savedcity.getId() > 0;
	}
	
	public boolean updateCity(City city){
		City savedcity = cityDAO.save(city);
		
		return savedcity.getId() > 0;
	}
	
	public boolean deleteCity(int cityId){
		City deletedcity = cityDAO.delete(cityId);
		
		return deletedcity.getId() > 0;
	}
	
	public City readCity(int cityId){
		return cityDAO.findOne(cityId);
	}
}
