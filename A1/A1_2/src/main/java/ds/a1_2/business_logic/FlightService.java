package ds.a1_2.business_logic;

import java.util.List;

import ds.a1_2.data_access.FlightDAO;
import ds.a1_2.model.Flight;

public class FlightService {
	private FlightDAO flightDAO;
	
	public FlightService(){
		flightDAO = new FlightDAO();
	}
	
	public List<Flight> findAll(){
		return flightDAO.findAll();
	}
	
	public boolean createFlight(Flight flight){
		Flight savedFlight = flightDAO.save(flight);
		
		return savedFlight.getId() > 0;
	}
	
	public boolean updateFlight(Flight flight){
		Flight savedFlight = flightDAO.update(flight);
		
		return savedFlight.getId() > 0;
	}
	
	public boolean deleteFlight(int flightId){
		Flight deletedFlight = flightDAO.delete(flightId);
		
		return deletedFlight.getId() > 0;
	}
	
	public Flight readFlight(int flightId){
		return flightDAO.findOne(flightId);
	}
}
