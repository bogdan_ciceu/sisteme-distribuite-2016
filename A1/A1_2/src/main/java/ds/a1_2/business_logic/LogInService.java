package ds.a1_2.business_logic;

import ds.a1_2.data_access.UserDAO;
import ds.a1_2.model.User;

public class LogInService {

	private UserDAO userDAO = new UserDAO();

	public User getUser(String username, String password) {

		User user = userDAO.findByUsername(username);
		if (user != null && user.getPassword().equals(password)) {
			return user;
		}

		return null;
	}

}
