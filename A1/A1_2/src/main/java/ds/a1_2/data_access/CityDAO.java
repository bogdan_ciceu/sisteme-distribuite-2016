package ds.a1_2.data_access;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import ds.a1_2.model.City;

public class CityDAO {

	private static final Log LOGGER = LogFactory.getLog(CityDAO.class);

	private SessionFactory sessionFactory;

	public CityDAO() {

		this.sessionFactory = new Configuration().configure().buildSessionFactory();
	}

	public City save(City city) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			int cityId = (Integer) session.save(city);
			city.setId(cityId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return city;
	}

	@SuppressWarnings("unchecked")
	public List<City> findAll() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<City> cities = null;
		try {
			tx = session.beginTransaction();
			cities = session.createQuery("FROM City").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return cities;
	}

	@SuppressWarnings("unchecked")
	public City findOne(int id) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<City> cities = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE id = :id");
			query.setParameter("id", id);
			cities = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return cities != null && !cities.isEmpty() ? cities.get(0) : null;
	}

	public City delete(int cityId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		City city = new City();
		try {
			tx = session.beginTransaction();
			city = (City) session.get(City.class, cityId);
			session.delete(city);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return city;
	}

}
