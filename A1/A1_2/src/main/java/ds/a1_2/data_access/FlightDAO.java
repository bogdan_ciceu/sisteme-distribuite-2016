package ds.a1_2.data_access;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import ds.a1_2.model.Flight;

public class FlightDAO {

	private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);
	private SessionFactory sessionFactory;

	public FlightDAO() {
		this.sessionFactory = new Configuration().configure().buildSessionFactory();
	}

	public Flight save(Flight flight) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			int flightId = (Integer) session.save(flight);
			flight.setId(flightId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
			flight.setId(0);
		} finally {
			session.close();
		}
		return flight;
	}
	
	public Flight update(Flight flight) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Flight dbFlight = (Flight)session.load(Flight.class, Integer.valueOf(flight.getId()));
			dbFlight.setFlightNumer(flight.getFlightNumer());
			dbFlight.setAirplaneType(flight.getAirplaneType());
			dbFlight.setDepartureCity(flight.getDepartureCity());
			dbFlight.setArrivalCity(flight.getArrivalCity());
			dbFlight.setDepartureDate(flight.getDepartureDate());
			dbFlight.setDepartureTime(flight.getDepartureTime());
			dbFlight.setArrivalDate(flight.getArrivalDate());
			dbFlight.setArrivalTime(flight.getArrivalTime());
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
			flight.setId(0);
		} finally {
			session.close();
		}
		return flight;
	}
	@SuppressWarnings("unchecked")
	public List<Flight> findAll() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flights;
	}

	@SuppressWarnings("unchecked")
	public Flight findOne(int id) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}

	public Flight delete(int flightId) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Flight flight = new Flight();
		try {
			tx = session.beginTransaction();
			flight = (Flight) session.get(Flight.class, flightId);
			flight.setDepartureCity(null);
			flight.setArrivalCity(null);
			tx.commit();
			tx = session.beginTransaction();
			flight = (Flight) session.get(Flight.class, flightId);
			session.delete(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null){
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flight;
	}

}
