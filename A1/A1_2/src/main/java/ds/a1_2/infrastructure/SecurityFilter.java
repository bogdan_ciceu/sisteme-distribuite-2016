package ds.a1_2.infrastructure;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ds.a1_2.infrastructure.constants.ApplicationConstants;
import ds.a1_2.infrastructure.constants.ApplicationPages;
import ds.a1_2.infrastructure.constants.ApplicationServlets;
import ds.a1_2.infrastructure.constants.Role;
import ds.a1_2.model.User;

public class SecurityFilter implements Filter {
	private static final Log LOGGER = LogFactory.getLog(SecurityFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOGGER.info("Security Filter Init");

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest r = (HttpServletRequest) request;

		LOGGER.info("Security Filter doFilter " + r.getRequestURI() + "     " + r.getRequestURL());

		 
		if (hasAccess(r)) {
			chain.doFilter(request, response);
		}else{
			RequestDispatcher requestDispatcher = request.getRequestDispatcher(ApplicationPages.NOT_ALLOWED);
			requestDispatcher.forward(request, response);
			//chain.doFilter(request, response);
		}

	}

	private boolean hasAccess(HttpServletRequest r) {
		//TODO revise logic for navigation to login page
		if (r.getRequestURI().equals("/A1_2/") ||
			r.getRequestURI().equals(ApplicationServlets.LOG_IN_SERVLET) ||
			SecurityUtilities.isResource(r)) {
			
			return true;
		}
		User u = (User) r.getSession().getAttribute(ApplicationConstants.SESSION_USER);
		if (u == null) {
			return false;
		} else {
			if (u.getRole().equals(Role.ADMIN)) {
				return SecurityUtilities.hasAdminAccess(r);
			}
			if (u.getRole().equals(Role.USER)) {
				return SecurityUtilities.hasUserAccess(r);

			}
		}

		return false;
	}

	@Override
	public void destroy() {

	}

}
