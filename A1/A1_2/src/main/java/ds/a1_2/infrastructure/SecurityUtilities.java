package ds.a1_2.infrastructure;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ds.a1_2.infrastructure.constants.ApplicationServlets;

public class SecurityUtilities {
	private static final Log LOGGER = LogFactory.getLog(SecurityUtilities.class);
	
	public static boolean hasAdminAccess(HttpServletRequest r){
		return r.getRequestURI().contains(ApplicationServlets.ADMIN_URIS_CONTAINS);
	}
	
	public static boolean hasUserAccess(HttpServletRequest r){
		return r.getRequestURI().contains(ApplicationServlets.USER_URIS_CONTAINS);
	}
	
	public static boolean isResource(HttpServletRequest r){
		Boolean result = r.getRequestURI().contains(ApplicationServlets.RESOURCES_URIS_CONTAINS);
		LOGGER.info("check is resource:" + r.getRequestURI()+ "  ==  " + result); 
		return result;
	}
}
