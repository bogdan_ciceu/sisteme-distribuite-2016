package ds.a1_2.infrastructure.constants;

public class ApplicationPages {
	public static final String LOG_IN = "WEB-INF/jsp/logIn.jsp";
	public static final String NOT_ALLOWED = "WEB-INF/jsp/fail.jsp";
	public static final String ADMIN_DASHBOARD = "WEB-INF/jsp/adminDashboard.jsp";
	public static final String USER_DASHBOARD = "WEB-INF/jsp/userDashboard.jsp";
	public static final String VIEW_FLIGHTS = "WEB-INF/jsp/viewFlights.jsp";
	public static final String VIEW_FLIGHT_MANAGER ="WEB-INF/jsp/flightManager.jsp";
 

}
