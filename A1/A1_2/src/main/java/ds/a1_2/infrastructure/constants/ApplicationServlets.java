package ds.a1_2.infrastructure.constants;

public class ApplicationServlets {
	public static final String USER_URIS_CONTAINS = "usr-";
	public static final String ADMIN_URIS_CONTAINS = "adm-";
	public static final String RESOURCES_URIS_CONTAINS = "resources";
	
	public static final String LOG_IN_SERVLET = "/A1_2/Login";
	
	public static final String USER_DASHBOARD = "/A1_2/usr-dashboard";
	public static final String USER_VIEW_FLIGHTS = "/A1_2/usr-ViewFlights";
	
	public static final String ADMIN_DASHBOARD = "/A1_2/adm-dashboard";
	
	
}
