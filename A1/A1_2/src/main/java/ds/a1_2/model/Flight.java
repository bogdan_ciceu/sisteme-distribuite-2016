package ds.a1_2.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

@Entity
public class Flight {

	@Id
	@GeneratedValue
	private int id;

	private int flightNumer;

	private String airplaneType;

	@ManyToOne
	@JoinColumn(name = "departureCity")
	private City departureCity;

	@ManyToOne
	@JoinColumn(name = "arrivalCity")
	private City arrivalCity;

	@Type(type = "text")
	private String departureDate;

	@Type(type = "text")
	private String departureTime;

	@Type(type = "text")
	private String arrivalDate;

	@Type(type = "text")
	private String arrivalTime;

	public Flight() {
		super();
	}

	public Flight(int flightNumer, String airplaneType, City departureCity, City arrivalCity, String departureDate,
			String departureTime, String arrivalDate, String arrivalTime) {
		super();
		this.flightNumer = flightNumer;
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.arrivalCity = arrivalCity;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
		this.arrivalDate = arrivalDate;
		this.arrivalTime = arrivalTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFlightNumer() {
		return flightNumer;
	}

	public void setFlightNumer(int flightNumer) {
		this.flightNumer = flightNumer;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public City getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}

	public City getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

}
