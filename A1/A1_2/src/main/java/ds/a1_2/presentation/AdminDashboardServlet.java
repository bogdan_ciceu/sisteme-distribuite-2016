package ds.a1_2.presentation;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ds.a1_2.business_logic.*;
import ds.a1_2.infrastructure.constants.*;
import ds.a1_2.model.*;

/**
 * Servlet implementation class AdminDashboardServlet
 */
public class AdminDashboardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private CityService cityService;
	private FlightService flightService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminDashboardServlet() {
		super();
		cityService = new CityService( );
		flightService = new FlightService();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			try {
				User user = (User) request.getSession().getAttribute("user");
				if (user.getRole().equals(Role.ADMIN)) {
					System.out.println("E ADMIN");

					List<City> cities = cityService.findAll();
					request.setAttribute("cities", cities);

					List<Flight> flights = flightService.findAll();
					request.setAttribute("flights", flights);

					RequestDispatcher requestDispatcher = request
							.getRequestDispatcher(ApplicationPages.ADMIN_DASHBOARD);
					requestDispatcher.forward(request, response);
				}
			} catch (Exception e) {

				RequestDispatcher requestDispatcher = request.getRequestDispatcher("fail");
				requestDispatcher.forward(request, response);
				e.printStackTrace();
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
