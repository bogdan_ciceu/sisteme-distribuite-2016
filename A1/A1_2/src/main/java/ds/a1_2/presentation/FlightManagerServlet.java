package ds.a1_2.presentation;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ds.a1_2.business_logic.*;
import ds.a1_2.infrastructure.constants.ApplicationPages;
import ds.a1_2.infrastructure.constants.ApplicationServlets;
import ds.a1_2.model.City;
import ds.a1_2.model.Flight;

/**
 * Servlet implementation class FlightManagerServlet
 */
public class FlightManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private CityService cityService;
	private FlightService flightService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FlightManagerServlet() {
		super();
		cityService = new CityService( );
		flightService = new FlightService();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try{
			List<City> cities = cityService.findAll();
			request.setAttribute("cities", cities);
			
			int flightId = Integer.parseInt(request.getParameter("flightId"));
			Flight flight= flightService.readFlight(flightId);
			
			if (flight.getId() < 1){
				flight.setFlightNumer(0);
				flight.setAirplaneType("");
				flight.setDepartureCity(null);
				flight.setArrivalCity(null);
				flight.setDepartureDate("");
				flight.setDepartureTime("");
				flight.setArrivalDate("");
				flight.setArrivalTime("");
			}
			request.setAttribute("flight", flight);
			
			request.getRequestDispatcher(ApplicationPages.VIEW_FLIGHT_MANAGER).forward(request, response);		
		
		} catch (Exception e) {
			request.getRequestDispatcher("fail").forward(request, response);
		}
	}
	
	/**
	 * create flight
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int flightNumber = Integer.parseInt(request.getParameter("number"));
		String airplaneType = request.getParameter("airplaneType");

		String dDateSpit[] = request.getParameter("departureDate").split(" ");
		String aDateSpit[] = request.getParameter("arrivalDate").split(" ");

		City departureCity = cityService.readCity(Integer.parseInt(request.getParameter("departureCity")));
		City arrivalCity = cityService.readCity(Integer.parseInt(request.getParameter("arrivalCity")));

		Flight flight = new Flight();
		flight.setFlightNumer(flightNumber);
		flight.setAirplaneType(airplaneType);
		flight.setDepartureCity(departureCity);
		flight.setArrivalCity(arrivalCity);
		flight.setDepartureDate(dDateSpit[0]);
		flight.setDepartureTime(dDateSpit[1]);
		flight.setArrivalDate(aDateSpit[0]);
		flight.setArrivalTime(aDateSpit[1]);
		
		if(flightService.createFlight(flight)){
			response.sendRedirect(ApplicationServlets.ADMIN_DASHBOARD);
		}
		else{
			RequestDispatcher requestDispatcher = request.getRequestDispatcher(ApplicationPages.NOT_ALLOWED);
			requestDispatcher.forward(request, response);
		}
	}
	
	/**
	 * update flight
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String httpMethod = request.getParameter("httpMethod");
		if (httpMethod.equals("put")){
			doPut(request,response);
		}
		else if (httpMethod.equals("delete")){
			doDelete(request,response);
		}
		else{		
			int flightId = Integer.parseInt(request.getParameter("flightId"));
			int flightNumber = Integer.parseInt(request.getParameter("number"));
			String airplaneType = request.getParameter("airplaneType");
	
			String dDateSpit[] = request.getParameter("departureDate").split(" ");
			String aDateSpit[] = request.getParameter("arrivalDate").split(" ");
	
			City departureCity = cityService.readCity(Integer.parseInt(request.getParameter("departureCity")));
			City arrivalCity = cityService.readCity(Integer.parseInt(request.getParameter("arrivalCity")));
	
			Flight flight = new Flight();
			flight.setId(flightId);
			flight.setFlightNumer(flightNumber);
			flight.setAirplaneType(airplaneType);
			flight.setDepartureCity(departureCity);
			flight.setArrivalCity(arrivalCity);
			flight.setDepartureDate(dDateSpit[0]);
			flight.setDepartureTime(dDateSpit[1]);
			flight.setArrivalDate(aDateSpit[0]);
			flight.setArrivalTime(aDateSpit[1]);
	
			if(flightService.updateFlight(flight)){
				response.sendRedirect(ApplicationServlets.ADMIN_DASHBOARD);
			}
			else{
				RequestDispatcher requestDispatcher = request.getRequestDispatcher(ApplicationPages.NOT_ALLOWED);
				requestDispatcher.forward(request, response);
			}
		}
	}
	
	/**
	 * delete flight
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int flightId = Integer.parseInt(request.getParameter("flightId"));
		
		if(flightService.deleteFlight(flightId)){
			response.sendRedirect(ApplicationServlets.ADMIN_DASHBOARD);
		}
		else{
			RequestDispatcher requestDispatcher = request.getRequestDispatcher(ApplicationPages.NOT_ALLOWED);
			requestDispatcher.forward(request, response);
		}
	}


}
