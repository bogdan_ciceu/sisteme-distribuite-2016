package ds.a1_2.presentation;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ds.a1_2.business_logic.LogInService;
import ds.a1_2.infrastructure.constants.ApplicationPages;
import ds.a1_2.infrastructure.constants.ApplicationServlets;
import ds.a1_2.infrastructure.constants.Role;
import ds.a1_2.model.User;

/**
 * Servlet implementation class LogInServlet
 */

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Log LOGGER = LogFactory.getLog(LoginServlet.class);
	LogInService logInService = new LogInService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	public LoginServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		 LOGGER.info(this.getClass()); 

		RequestDispatcher requestDispatcher = request.getRequestDispatcher(ApplicationPages.LOG_IN);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User user = logInService.getUser(username, password);

		try {
			if (user.getRole().equals(Role.ADMIN)) {
				request.getSession().setAttribute("user", user);
				response.sendRedirect(ApplicationServlets.ADMIN_DASHBOARD);

			} else if (user.getRole().equals(Role.USER)) {
				request.getSession().setAttribute("user", user);
				response.sendRedirect(ApplicationServlets.USER_DASHBOARD);
				//request.getRequestDispatcher(ApplicationServlets.USER_DASHBOARD).forward(request, response);
			} else {
				request.getRequestDispatcher(ApplicationPages.NOT_ALLOWED).forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			RequestDispatcher requestDispatcher = request.getRequestDispatcher(ApplicationPages.NOT_ALLOWED);
			requestDispatcher.forward(request, response);
		}

	}

}
