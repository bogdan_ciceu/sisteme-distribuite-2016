package ds.a1_2.presentation;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ds.a1_2.business_logic.FlightService;
import ds.a1_2.infrastructure.constants.ApplicationPages;
import ds.a1_2.model.Flight;

public class UserDashboardServlet extends HttpServlet {

	//private static final Log LOGGER = LogFactory.getLog(UserDashboardServlet.class);
	private static final long serialVersionUID = 1L;
	private FlightService flightService;

	public UserDashboardServlet() {
		flightService = new FlightService();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		System.out.println("doGet - UserDashboardServlet");
		List<Flight> flights = flightService.findAll();
		request.setAttribute("flights", flights);
		request.getRequestDispatcher(ApplicationPages.USER_DASHBOARD).forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {}

}
