<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>DS Assignment 1.2</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/logIn.css"
	media="screen" type="text/css" />
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

</head>

<body>

	<div id="manager">
		<p>
			<a href="/FlightManager">FLIGHT MANAGER</a><span
				class="fontawesome-arrow-right"></span> <a href="/CityManager">
				CITY MANAGER</a><span class="fontawesome-arrow-right"></span>
		</p>
	</div>
	<table
		class="table table-bordered table-striped table-striped-column-2"
		id="table">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Latitude</th>
				<th>Longitude</th>
			</tr>
		</thead>
		<tbody>

			<c:forEach var="city" items="${cities}">
				<tr>
					<td>${city.id}</td>
					<td>${city.name}</td>
					<td>${city.latitude}</td>
					<td>${city.longitude}</td>
				</tr>
			</c:forEach>


		</tbody>
	</table>

	<table
		class="table table-bordered table-striped table-striped-column-2"
		id="table">
		<thead>
			<tr>
				<th>#</th>
				<th>Number</th>
				<th>Airplane Type</th>
				<th>Departure City</th>
				<th>Departure Date</th>
				<th>Arrival City</th>
				<th>Arrival Date</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>

			<c:forEach var="flight" items="${flights}">
				<tr>
					<td>${flight.id}</td>
					<td>${flight.flightNumer}</td>
					<td>${flight.airplaneType}</td>
					<td>${flight.departureCity.name}</td>
					<td>${flight.departureDate}${flight.departureTime}</td>
					<td>${flight.arrivalCity.name}</td>
					<td>${flight.arrivalDate}${flight.arrivalTime}</td>
					<td>
						<a href="adm-flight?flightId=${flight.id}">
							<span class="fontawesome-edit link"></span>
						</a>
					</td>
				</tr>
			</c:forEach>


		</tbody>
	</table>


</body>
</html>