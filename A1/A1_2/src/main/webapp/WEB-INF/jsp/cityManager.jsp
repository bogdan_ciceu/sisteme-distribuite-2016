<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>DS Assignment 1.2</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/logIn.css"
	media="screen" type="text/css" />
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

</head>

<body>
	<div id="manager">
		<p>
			<a href="/FlightManager">FLIGHT MANAGER</a><span
				class="fontawesome-arrow-right"></span> <a href="/CityManager">
				CITY MANAGER</a><span class="fontawesome-arrow-right"></span>
		</p>
	</div>
	<div class="container">
		<div id="login">

			<form action="CityManager" method="post">
				<p>
					<span class="fontawesome-home"></span><input type="text"
						value="Name" name="name"
						onBlur="if(this.value == '') this.value = 'Name'"
						onFocus="if(this.value == 'Name') this.value = ''" required>
				</p>

				<p>
					<span class="fontawesome-resize-horizontal"></span><input
						type="text" value="Latitude" name="latitude"
						onBlur="if(this.value == '') this.value = 'Latitude'"
						onFocus="if(this.value == 'Latitude') this.value = ''" required>
				</p>

				<p>
					<span class="fontawesome-resize-vertical"></span><input type="text"
						value="Longitude" name="longitude"
						onBlur="if(this.value == '') this.value = 'Longitude'"
						onFocus="if(this.value == 'Longitude') this.value = ''" required>
				</p>
				<input type="submit" value="ADD CITY">


			</form>

			<p>
				Not a member? <a href="#">CITY MANAGER</a><span
					class="fontawesome-arrow-right"></span>
			</p>

		</div>
		<!-- end login -->

	</div>

</body>
</html>