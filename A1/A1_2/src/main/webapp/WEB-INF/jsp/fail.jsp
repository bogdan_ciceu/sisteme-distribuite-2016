<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>DS Assignment 1.2</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/logIn.css"
	media="screen" type="text/css" />
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

</head>

<body>
	<div class="container">
		<div id="login">
			<form action="Login" method="post">
				<fieldset class="clearfix">

					<div class="fail">FAIL !</div>

				</fieldset>

			</form>

			<p>
				Not logged in? <a href="Login">Login</a><span
					class="fontawesome-arrow-right"></span>
			</p>

		</div>
		<!-- end login -->

	</div>

</body>
</html>