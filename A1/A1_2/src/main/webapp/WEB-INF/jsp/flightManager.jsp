<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>DS Assignment 1.2</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/logIn.css"
	media="screen" type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/jquery.datetimepicker.css"
	media="screen" type="text/css" />
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.datetimepicker.full.min.js"></script>
</head>

<body>
	
		
		<table
		class="table table-bordered table-striped table-striped-column-2"
		id="table">
		<thead>
			<tr>
			<th></th>
				<th>#</th>
				<th>Number</th>
				<th>Airplane Type</th>
				<th>Departure City</th>
				<th>Departure Date</th>
				<th>Arrival City</th>
				<th>Arrival Date</th>
			</tr>
		</thead>
		<tbody>
				<tr>
					<td>Read flight:</td>
					<td>${flight.id}</td>
					<td>${flight.flightNumer}</td>
					<td>${flight.airplaneType}</td>
					<td>${flight.departureCity.name}</td>
					<td>${flight.departureDate}${flight.departureTime}</td>
					<td>${flight.arrivalCity.name}</td>
					<td>${flight.arrivalDate}${flight.arrivalTime}</td>
				</tr>
		</tbody>
		</table>
	
	<div class="container">
	<div class="row"> 
		<div class="col-sx-3">
			<div id="login">
					<form action="adm-flight" method="post">
					<input type="hidden" name="httpMethod" value="put" />
					<p>create flight:</p>
						<p>
							<span class="fontawesome-dashboard"></span><input type="text"
								value="Number" name="number"
								onBlur="if(this.value == '') this.value = 'Number'"
								onFocus="if(this.value == 'Number') this.value = ''" required>
						</p>
		
						<p>
							<span class="fontawesome-plane"></span><input type="text"
								value="Airplane Type" name="airplaneType"
								onBlur="if(this.value == '') this.value = 'Airplane Type'"
								onFocus="if(this.value == 'Airplane Type') this.value = ''"
								required>
						</p>
		
						<p>
							<span class="fontawesome-building"></span><select
								id="departureFlight" name="departureCity">
								<c:forEach var="city" items="${cities}">
									<option value="${city.id }">${city.name }</option>
								</c:forEach>
							</select>
						</p>
						<span class="fontawesome-calendar"></span><input class="datetimepicker"
							placeholder="Departure date" type="text" name="departureDate">
		
						<p>
							<span class="fontawesome-building"></span> <select
								 name="arrivalCity">
								<c:forEach var="city" items="${cities}">
									<option value="${city.id }">${city.name }</option>
								</c:forEach>
							</select>
						</p>
		
						<span class="fontawesome-calendar"></span>
						<input class="datetimepicker" placeholder="Arrival date" type="text" name="arrivalDate"> 
							
						<input type="submit" value="ADD FLIGHT">
		
					</form>

			</div>
				<!-- end login -->
		</div>
		<div class="col-sx-3">
			<div id="login">
					<form action="adm-flight" method="post">
						<p>Update flight</p>
						<input type="hidden" name="httpMethod" value="post" />
						<input type="hidden" name="flightId" value="${flight.id }" />
						
						<p>
							<span class="fontawesome-dashboard"></span><input type="text" readonly="readonly"
								name="number" value="${flight.flightNumer }"
								 required>
						</p>
		
						<p>
							<span class="fontawesome-plane"></span><input type="text"
								value="Airplane Type" name="airplaneType"
								onBlur="if(this.value == '') this.value = 'Airplane Type'"
								onFocus="if(this.value == 'Airplane Type') this.value = ''"
								required>
						</p>
		
						<p>
							<span class="fontawesome-building"></span><select
								id="departureFlight" name="departureCity">
								<c:forEach var="city" items="${cities}">
									<option value="${city.id }">${city.name }</option>
								</c:forEach>
							</select>
						</p>
						
						<span class="fontawesome-calendar"></span>
						<input class="datetimepicker" placeholder="Departure date" type="text" name="departureDate">
		
						<p>
							<span class="fontawesome-building"></span> <select
								id="arrivalFlight" name="arrivalCity">
								<c:forEach var="city" items="${cities}">
									<option value="${city.id }">${city.name }</option>
								</c:forEach>
							</select>
						</p>
		
						<span class="fontawesome-calendar"></span>
						<input class="datetimepicker" placeholder="Arrival date" type="text"name="arrivalDate"> 
						
						<input type="submit" value="Update FLIGHT">
		
					</form>
		
				</div>
				<!-- end login -->
		</div>
		<div class="col-sx-3">
		<div id="login">
				<form action="adm-flight" method="post">
					<p>delete flight</p>
						<input type="hidden" name="httpMethod" value="delete" />
						<input type="hidden" name="flightId" value="${flight.id }" />
						<p>
							<span class="fontawesome-dashboard"></span><input type="text" readonly="readonly"
								name="number" value="${flight.flightNumer }"
								 required>
						</p>
						
						<input type="submit" value="Delete FLIGHT">
				</form>
	
			</div>
			<!-- end login -->
	</div>
	</div>
	</div>

</body>
<script type="text/javascript">
	jQuery('.datetimepicker').datetimepicker();
</script>
</html>