<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>DS Assignment 1.2</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/logIn.css"
	media="screen" type="text/css" />
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

</head>

<body>
	<div class="container">
		<div id="login">
			<div id="logo">DS</div>
			<div id="miniLogo">ASSIGNMENT A1.2</div>
			<form action="Login" method="post">
				<fieldset class="clearfix">

					<p>
						<span class="fontawesome-user"></span><input type="text"
							value="Username" name="username"
							onBlur="if(this.value == '') this.value = 'Username'"
							onFocus="if(this.value == 'Username') this.value = ''" required>
					</p>
					<!-- JS because of IE support; better: placeholder="Username" -->
					<p>
						<span class="fontawesome-lock"></span><input type="password"
							value="Password" name="password"
							onBlur="if(this.value == '') this.value = 'Password'"
							onFocus="if(this.value == 'Password') this.value = ''" required>
					</p>
					<!-- JS because of IE support; better: placeholder="Password" -->
					<p>
						<input type="submit" value="Sign In">
					</p>

				</fieldset>

			</form>

			<p>
				Not a member? <a href="#">Sign up now</a><span
					class="fontawesome-arrow-right"></span>
			</p>

		</div>
		<!-- end login -->

	</div>

</body>
</html>