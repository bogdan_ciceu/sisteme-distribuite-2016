<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DS Assignment 1.2</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/logIn.css"
	media="screen" type="text/css" />
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

</head>
<body>
<table class="table table-bordered table-striped table-striped-column-2"
		id="table">
		<thead>
			<tr>
				<th>#</th>
				<th>Number</th>
				<th>Airplane Type</th>
				<th>Departure City</th>
				<th>Departure Date</th>
				<th>Arrival City</th>
				<th>Arrival Date</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>

			<c:forEach var="flight" items="${flights}">
				<tr>
					<td>${flight.id}</td>
					<td>${flight.flightNumer}</td>
					<td>${flight.airplaneType}</td>
					<td>${flight.departureCity.name}</td>
					<td>${flight.departureDate}${flight.departureTime}</td>
					<td>${flight.arrivalCity.name}</td>
					<td>${flight.arrivalDate}${flight.arrivalTime}</td>
					<td><a href="/FlightManager"><span
							class="fontawesome-trash link"></span></a> <a href="/FlightManager"><span
							class="fontawesome-edit link"></span></a></td>
				</tr>
			</c:forEach>


		</tbody>
	</table>
</body>
</html>