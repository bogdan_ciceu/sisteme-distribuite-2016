<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>View Flights</title>
</head>
<body>
	<h1>View Flights</h1>
	<c:forEach var="flight" items="${flights}">
		
		<div class="row">
			<div class="col-sm-1">${flight.id}</div>
			<div class="col-sm-2">${flight.flightNumer}</div>
			<div class="col-sm-2">${flight.airplaneType}</div>
			<div class="col-sm-2">${flight.departureCity}</div>
			<div class="col-sm-2">${flight.arrivalCity}</div>
			<div class="col-sm-2">${flight.departureDate}</div>
			<div class="col-sm-2">${flight.departureTime}</div>
			<div class="col-sm-2">${flight.arrivalDate}</div>
			<div class="col-sm-2">${flight.arrivalTime}</div>
		</div>
	</c:forEach>
</body>
</html>