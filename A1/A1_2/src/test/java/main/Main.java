package main;

import ds.a1_2.model.City;
import ds.a1_2.data_access.*;
import ds.a1_2.model.*;

public class Main {

	public static void main(String[] args) {
		City c = new City();
		c.setName("Bucuresti");
		CityDAO cityDao = new CityDAO();
		City c1= cityDao.save(c);
		c = new City();
		c.setName("Cluj-Napoca");
		City c2 = cityDao.save(c); 
		
		FlightDAO fDao = new FlightDAO();
		fDao.save(new Flight(5251, "Boeing69A", c1, c2, "25.09.2016", "2:55:50", "25.09.2016", "4:55:50"));
	}
}
