package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
/**
 * Created by bcice_000 on 07.11.2016.
 */
public interface IPriceSelling {

    double computePriceSelling(Car c);
}
