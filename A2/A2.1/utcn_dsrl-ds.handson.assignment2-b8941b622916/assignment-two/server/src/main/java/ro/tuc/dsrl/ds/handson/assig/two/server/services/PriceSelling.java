package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceSelling;
/**
 * Created by bcice_000 on 07.11.2016.
 */
public class PriceSelling implements IPriceSelling {

    public double computePriceSelling(Car c){
        return c.getPurchasingPrice() - (c.getPurchasingPrice()/7*(2015 - c.getYear()));
    }
}
