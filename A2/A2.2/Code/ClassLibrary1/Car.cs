﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Serializable]
    public class Car
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public int EngineSize { get; set; }
        public double InitialPrice { get; set; }
    }
}
