﻿using Model;
using RemotableObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Client : Form
    {
        private RemotableCarService remoteObject;
        private Car Car;

        public Client()
        {
            InitializeComponent();

            //************************************* TCP *************************************//
            // using TCP protocol
            // running both client and server on same machines
            TcpChannel chan = new TcpChannel();
            ChannelServices.RegisterChannel(chan);
            // Create an instance of the remote object
            remoteObject = (RemotableCarService)Activator.GetObject(typeof(RemotableCarService), "tcp://localhost:8080/HelloWorld");
            // if remote object is on another machine the name of the machine should be used instead of localhost.
            //************************************* TCP *************************************//

            //Car tesla = new Car()
            //{
            //    Name = "Tesla",
            //    Year = 2014,
            //    InitialPrice = 20000,
            //    EngineSize = 2000
            //};

            //double tax = remoteObject.computeTax(tesla);
            //Debug.WriteLine(string.Format("tax computed for {0} is {1}", tesla.Name, tax));

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name = this.type.Text;
            double price;
            int year, engineSize;

            double.TryParse(this.initPrice.Text, out price);
            int.TryParse(this.year.Text, out year);
            int.TryParse(this.size.Text, out engineSize);

            var car = new Car()
                    {
                        Name = name,
                        InitialPrice = price,
                        EngineSize = engineSize,
                        Year = year
                    };
            double priceSelling = remoteObject.computePriceSelling(car);
            double tax = remoteObject.computeTax(car);

            this.sellingPrice.Text = priceSelling.ToString();
            this.tax.Text = tax.ToString();
        }
    }
}
