using System;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using Model;

namespace RemotableObjects
{
	public class RemotableCarService : MarshalByRefObject
	{
		public RemotableCarService(){}

        public double computeTax(Car c)
        {
            if (c.EngineSize <= 0)
            {
                throw new Exception("Engine capacity must be positive.");
            }
            int sum = 8;
            if (c.EngineSize > 1601) sum = 18;
            if (c.EngineSize > 2001) sum = 72;
            if (c.EngineSize > 2601) sum = 144;
            if (c.EngineSize > 3001) sum = 290;

            return c.EngineSize / 200.0 * sum;
        }

        public double computePriceSelling(Car c)
        {
            return c.InitialPrice - (c.InitialPrice / 7 * (2016 - c.Year));
        }

        public bool SetMessage(string message)
		{
			Cache.GetInstance().MessageString = message;

            return true;
		}

	}
}
