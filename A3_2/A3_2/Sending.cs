﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using System.Web.Script.Serialization;
using Model;

namespace A3_2
{
    class Sending
    {
        private static IModel channel;
        static void Main(string[] args)
        {
            channel = new ConnectionFactory() { HostName = "localhost" }
                        .CreateConnection()
                        .CreateModel();
            channel.QueueDeclare(
                    queue: "hello",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null
                );

            var dvd = new DVD()
            {
                Name = "dvd1",
                Year = 1990,
                Price = 10.9
            };
            Publish(dvd);

            dvd.Name = "dvd2";
            Publish(dvd);

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        private static void Publish(DVD dvd)
        {
            string message = new JavaScriptSerializer().Serialize(dvd);
            var body = Encoding.UTF8.GetBytes(message);
            Console.WriteLine(" [x] Sent {0}", message);
            channel.BasicPublish(exchange: "",
                         routingKey: "hello",
                         basicProperties: null,
                         body: body);
        }
    }
}
