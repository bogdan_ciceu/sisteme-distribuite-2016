﻿using Model;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace FormSender
{
    public partial class Sender : Form
    {
        private static IModel channel;
        public Sender()
        {
            InitializeComponent();

            channel = new ConnectionFactory() { HostName = "localhost" }
                       .CreateConnection()
                       .CreateModel();
            channel.QueueDeclare(
                    queue: "hello",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null
                );
        }

        private static void Publish(DVD dvd)
        {
            string message = new JavaScriptSerializer().Serialize(dvd);
            var body = Encoding.UTF8.GetBytes(message);
            MessageBox.Show(message);
            channel.BasicPublish(exchange: "",
                         routingKey: "hello",
                         basicProperties: null,
                         body: body);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int year;
            double price;

            Int32.TryParse(this.txt_year.Text, out year);
            Double.TryParse(this.txt_price.Text, out price);
            
            Publish(new DVD()
            {
                Name = this.txt_name.Text,
                Year = year,
                Price = price
            });
        }
    }
}
