﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class DVD
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public double Price { get; set; }
        
        public override string ToString()
        {
            return string.Format("Name: {0} ; Year: {1}; Price: {2}", Name, Year, Price); 
        }
    }
}
