﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Web.Script.Serialization;
using Model;
using System.Net.Mail;
using System.IO;

namespace Receiver
{
    class Recieving
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    DVD recievedDVD = new JavaScriptSerializer().Deserialize<DVD>(message);
                    Console.WriteLine(" [x] Received {0}", recievedDVD.ToString());
                    SendMail(recievedDVD);
                    WriteInFile(recievedDVD);
                };
                channel.BasicConsume(queue: "hello",
                                     noAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        private static void SendMail(DVD dvd)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("bogdanciceutesting@gmail.com");
                mail.To.Add("b.ciceu@gmail.com");
                mail.Subject = "New DVD: " + dvd.Name;
                mail.Body = string.Format("A new DVD was added, DVD informations:{0}", dvd.ToString());

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("bogdanciceutesting@gmail.com", "testing.testing");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                Console.WriteLine("mail sent");
            }
            catch (Exception ex)
            {
                Console.WriteLine("error mail" + ex.Message);
            }
        }

        private static void WriteInFile(DVD dvd)
        {
            try
            {
                string path = @"E:\Facultate\An_4\SD\a3\A3_2\Receiver\output.txt";
                if (!File.Exists(path))
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine("DVDs Recieved");
                    }
                }

                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine("A new DVD was added, DVD informations: " + dvd.ToString());
                    sw.WriteLine("");
                }

                Console.WriteLine("writed in file");
            }
            catch (Exception ex)
            {
                Console.WriteLine("error write in file" + ex.Message);
            }
        }
    }
}
